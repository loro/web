(function(angular, undefined) {
'use strict';

angular.module('loroApp.constants', [])

.constant('appConfig', {userRoles:['guest','user','admin']})

;
})(angular);