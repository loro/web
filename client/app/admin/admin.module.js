'use strict';

angular.module('loroApp.admin', [
  'loroApp.auth',
  'ui.router'
]);
