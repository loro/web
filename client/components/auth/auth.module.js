'use strict';

angular.module('loroApp.auth', [
  'loroApp.constants',
  'loroApp.util',
  'ngCookies',
  'ui.router'
])
  .config(function($httpProvider) {
    $httpProvider.interceptors.push('authInterceptor');
  });
