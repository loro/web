# Loro | Getting Started!

## Prerequisites

- [Git](https://git-scm.com/)
- [Node.js](nodejs.org) - Install v5.4.1 Stable. This will also install NPM.
- Bower: Type this command in the command prompt or terminal: `npm install -g bower`
- Grunt: Type this command in the command prompt or terminal: `npm install -g grunt-cli`
- [MongoDB](https://www.mongodb.org/)

## Other things you __want__ to have

- [TortoiseGit](https://tortoisegit.org/download/) - For source control
- [Robomongo](http://app.robomongo.org/download.html) - With this, you will be able to see the collections created in MongoDB.
- [Visual Studios Express](https://www.microsoft.com/en-us/download/details.aspx?id=44914) - Just do it
- [Python 2.7.11](https://www.python.org/downloads/) - Download Python 2.7.11 and make sure that your system environment variable is set (for windows).

## Set up SSH, put it in GitLab, and clone this repo!

### Set up SSH key.

- Open command prompt or terminal.

1. Check if you already have an ssh key by:
    - `cd ~/.ssh`
    - `ls`
        - If you see an ____id_rsa.pub____ file, this means that you already have it set! Skip to step: 4.
2. Create the ssh key by:
    - `ssh-keygen -t rsa -b 4096 -C "your_email@example.com"`
3. Press enter throughout the prompts until it doesn't ask you anything anymore. It should be 3 times you press enter.
4. Copy the ____id_rsa.pub____ to your clipboard.
    - For __Mac__: `pbcopy < ~/.ssh/id_rsa.pub`
    - For __Windows__: `clip < ~/.ssh/id_rsa.pub`
    - For __Linux__: 
        - `sudo apt-get install xclip`: This will download and install xclip. If you don't have `apt-get`, you might need to use another installer (like `yum`).
        - `xclip -sel clip < ~/.ssh/id_rsa.pub`: This will copy it.
    
### Put it in GitLab

1. Go to GitLab's SSH Keys: [click me](https://gitlab.com/profile/keys)
2. Click on Add SSH Key
3. Paste your SSH key and give it a name. Normally, you'd use your computer name.
4. Click on Add Key.

### Clone this repo

- Open the command prompt or terminal again.

1. Go to where you'd like to have this project. Usually, I have a folder called Projects and once you `cd` in there, you can clone this repo.
2. Clone it! `git clone git@gitlab.com:loro/web.git`
3. You're done!

## Git Tips

- Make sure you `git pull` often. If you run into an error where git cannot merge for you, go to slack and we'll talk about it!
- Before committing, do `git status`, so you can see all of the changes that you've made to the project.
- Do `git add -A && git commit -m "what did I do"` to add and commit what you've done.
    - `-A` means that you're selecting everything (A for All). Instead of `-A`, you can choose which file you want to commit and give it a specific message to what you've done in that file.
    - ___Example___: `git add client/app/main/main.html && git commit -m "changed the title of the page"`
- Do `git push` to push into GitLab for everyone to see your commits!

## Developing

1. Run `npm install` to install server dependencies.

2. Run `bower install` to install front-end dependencies.

3. Run `mongod` in a separate shell to keep an instance of the MongoDB Daemon running.

4. Run `grunt serve` to start the development server. It should automatically open the client in your browser when ready.

# Build & development

Run `grunt build` for building and `grunt serve` for preview.

# Testing

Running `npm test` will run the unit tests with karma.
